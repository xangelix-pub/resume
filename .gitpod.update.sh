#!/usr/bin/env bash

curl -fsSL https://gitlab.com/Xangelix/gitpod-signed-workspace/-/raw/master/templates/template_loader.sh -o ./template_loader.sh
sudo chmod +x ./template_loader.sh
./template_loader.sh $1 $2
rm -f ./template_loader.sh
